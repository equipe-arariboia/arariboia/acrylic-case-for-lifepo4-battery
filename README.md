# Acrylic case for LiFePO4 battery

Projeto de encapsulamento de acrílico de 8 mm de espessura para pack de bateira de lítio LiFePO4. 


## O que é?

Projeto para encapsulamento de pack de bateria de lítio, garantindo fixação e proteção às células de lítio.

## Por que fazer?

Caso possua um grande pack de bateria pode-se alterar o projeto 3D aqui proposto para sua necessidade.
O acrílico é um material transparente, portanto é possível ver as células, sendo possível verificar se há alguma avaria.

## Como foi feito?


1. Modelagem 3D do pack de baterias a partir do modelo de célula LiFePO4;

![Desenhos 2D para corte](https://gitlab.com/equipe-arariboia/arariboia/acrylic-case-for-lifepo4-battery/-/raw/master/Fotos/Modelo%20bateria%20renderizado.png)

2. Modelagem 3D de encapsulamento a partir do modelo 3D do pack;

![Desenhos 2D para corte](https://gitlab.com/equipe-arariboia/arariboia/acrylic-case-for-lifepo4-battery/-/raw/master/Fotos/Modelo%20Encapsulamento%20e%20Bateria%20competo.png)

3. Planificação dos modelos 3D das faces do encapsulamento;

![Desenhos 2D para corte](https://gitlab.com/equipe-arariboia/arariboia/acrylic-case-for-lifepo4-battery/-/raw/master/Fotos/Desenho%20Completo.png)

4. Corte a lazer das chapas de acrílico utilizando os desenhos 2D;



5. Montagem e finalização do projeto;

![Desenhos 2D para corte](https://gitlab.com/equipe-arariboia/arariboia/acrylic-case-for-lifepo4-battery/-/raw/master/Fotos/_DSC0680.JPG)

![Desenhos 2D para corte](https://gitlab.com/equipe-arariboia/arariboia/acrylic-case-for-lifepo4-battery/-/raw/master/Fotos/_DSC0703.JPG)


## Material utilizado

- Chapa de acrílico transparente de 8 mm de espesurra;
- Barra roscada M6 (1 metro = 4 pedaços de 25 cm)
- 6 parafusos M6
- 14 porcas autotravante M6